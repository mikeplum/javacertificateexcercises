package Examples.e;

public class InitTest{
        public InitTest(){
            s1 = sM1("1");
        };

        static String s1 = sM1("Examples/a");
        String s3 = sM1("2");

        {
            s1 = sM1("3");
        }

        static{
            s1 = sM1("Examples/b");
        }

        static String s2 = sM1("Examples/c");

        String s4 = sM1("4");


        public static void main(String args[]){
            InitTest it = new InitTest();
            Integer a = new Integer(5);
            a=a++;
            new InitTest();
        }

        private static String sM1(String s){
            System.out.println(s);  return s;
        }
}
