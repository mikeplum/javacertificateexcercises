package Examples.d;

public interface Predicate<T> {
    boolean test(T t);
}
