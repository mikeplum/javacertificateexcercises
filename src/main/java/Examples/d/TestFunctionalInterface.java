package Examples.d;

import java.util.ArrayList;

public class TestFunctionalInterface {

    public ArrayList<Smth> list = new ArrayList<Smth>();

    TestFunctionalInterface(){
        for(int i = 0; i < 10; i++){
            list.add(new Smth(i));
        }
    }

    void printData(ArrayList<Smth> dataList, Predicate<Smth> p){
        for (Smth d: dataList){
            if(p.test(d)) System.out.println(d.value);
        }
    }

    public static void main(String[] args){
        TestFunctionalInterface tfi = new TestFunctionalInterface();
        tfi.printData(tfi.list, (Smth d) -> {return d.value > 1;});
        System.out.println(tfi.list.get(1).value);
    }

}
