package Examples.c;

import Examples.b.AccessTester;

public class Ex {

    int instance;
    static float floatvar;
    static Object objectStatic;
    Object objectInstance;
    native float getVariance() throws Exception;

    public void switchmetod(int a){

        System.out.println(instance);
        System.out.println(floatvar);
        System.out.println(objectStatic);
        System.out.println(objectInstance);
        byte x = 127;

        switch (x){

            case 'a' :
                break;
            case 'b' :
                break;
            case 'c' :
                break;
            case 'd' :
                break;
            case 'e' :
                break;
            case 'f' :
                break;
            case 'g' :
                break;
            case 'h' :
                break;
            case 'i' :
                break;
            case 'j' :
                break;
            case 'k' :
                System.out.println("dupa");
            case 'l' :
                System.out.println("dupa");
                break;
        }

    }


    public static void main(String[] args) throws ClassNotFoundException {

        Ex ex = new Ex();
        ex.switchmetod(107);
        byte by = 10;
        int integer = 2;
        System.out.println(by*integer);
        Class.forName("java.lang.String");

        AccessTester at = new AccessTester();

        Boolean b = false;
        String str = "abc";
        System.out.println(str+=b);

        char c = '1';
        int i = 2;
        System.out.println(c += i);

        String str1="OK";
        String str2="OK";
        System.out.println(str1.hashCode());
        System.out.println(str2.hashCode());
        if(str1.equals(str2)){
            System.out.println("SAME");
        }else{
            System.out.println("NOT-SAME");
        }

        Integer i1 = 1;
        Integer i2 = new Integer(1);

        if(i1 == i2){
            System.out.println("same1");
        }
        if(i1.equals(i2)){
            System.out.println("same2");
        }
    }
}