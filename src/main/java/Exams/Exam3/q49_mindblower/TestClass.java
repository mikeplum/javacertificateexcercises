package Exams.Exam3.q49_mindblower;

//What would be the result of attempting to compile and run the following program?

class TestClass{
    static TestClass ref;
    String[] arguments;

    public static void main(String args[]){
        ref = new TestClass();
        ref.func(args);
        ref.func2(args);
    }
    public void func(String[] args){
        ref.arguments = args;
    }
    private void func2(String[] args){
        ref.arguments = args;
    }
}



