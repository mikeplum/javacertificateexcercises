package Exams.Exam3.q46;

class A {
    public static void sM1() {
        System.out.println("In base static");
    }
}

class B extends A {
    //public static void sM1() {  System.out.println("In sub static"); }

    //public  void sM1() {  System.out.println("In sub non-static"); }

    //---DEF: static method can be shadowed by a static method in the subclass.
    //---DEF: Inner call cannot declare an static method.
    //---DEF: static method cannot be overridden by a non-static method and vice versa
    //---DEF: two metods of same signature cannot exists - even static and non-static | public static void sM1(){}; public  void sM1() { }
}


