package Exams.Exam3.q46_important;

class A {
    public int i = 10;
    public static void m1() {
        System.out.println("m1 super-class");
    }
    public void m2() {
        System.out.println(i);
    }
}

class B extends A {
    public int i = 20;
    public static void m1() {
        System.out.println("m1 sub-class");
    }
    public void m2() {
        System.out.println(i);
    }
}
public class TestPolimorphizm{
    public static void main(String[] args) {
        A a  = new B(); // LINE1
        System.out.println(((B)a).i); //castin are done therefore it is treat as type B wast declared in LINE1
        System.out.println(a.i);  //will print 10 instead of 20
        a.m1();  //will call A's m1
        a.m2();  //will call B's m2 as m2() is not static and so overrides A's m2()
    }

}