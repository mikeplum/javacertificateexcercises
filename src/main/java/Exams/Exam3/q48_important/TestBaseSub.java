package Exams.Exam3.q48_important;

import java.util.ArrayList;
import java.util.List;

abstract class Base{
    final Object[ ] objArr = { null } ;
    void methodA(){
        System.out.println("base - MethodA");
    }

    public Base(){

    }

    private Base(int value){

    }

}

class Sub extends Base{


    public void methodA(){
        System.out.println("sub - MethodA");
    }
    public void methodB(){
        System.out.println("sub - MethodB");
    }

    public static void main(String args[]){
        Base b=new Sub();
        List<String> list = new ArrayList<String>();
        b.methodA();
        ArrayList listOfStrings = null;

        //b.methodB();  // b is reference to Type Base which has no methodB declaration uncomment this and compilation error occours
    }

}