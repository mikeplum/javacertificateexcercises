package Exams.Exam3.q51;

import java.io.*;

public class TestClassThrows {

    void myMethod(int num) throws IOException, ClassNotFoundException {
        if (num == 1)
            throw new IOException("IOException Occurred");
        else
            throw new ClassNotFoundException("ClassNotFoundException");
    }


    public static void main(String args[]) {
        try {
            TestClassThrows obj = new TestClassThrows();
            obj.myMethod(1);
        } catch (IOException ex) {
            System.out.println(ex);
        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
        }
    }
}

