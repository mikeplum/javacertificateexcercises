package Exams.Exam3.q13;

class Game {
    public void play() throws Exception   {
        System.out.println("Playing...");
    }
    public void play(String ball)    {
        System.out.println("Playing Game with "+ball);
    }
}

class Soccer extends Game {
    public void play(String ball)    {
        System.out.println("Playing Soccer with "+ball);
    }
}

public class TestClass {
    public static void main(String[] args) throws Exception  {
        Game g = new Game();
        g.play("aaa");
        ((Game)g).play("bbb");

        Soccer s = (Soccer) g;
        s.play("ccc");
    }
}