package Exams.Exam3.q66;

public class TypeTest {

    public final int ale = 10; // must be explicitly initialized - can be done  in constructor, instance block or in place
    public int ale2; // ale is already defined in class it is optional to initialize

    class inner{
        public final int ale = 20; //in inner class it can be anther final variable with the same name as variable in public class
    }

    public static void main(String[] args) {
        //double a = 10,000,000.0; //Comma is not a valid character here.
        //double b = 10-000-000; //Dash (-) is not a valid character here. //compilatior sais that can be replaced by 10
        double c = 10_000_000; //valid 10 000 000
        //double d = 10 000 000; //expected ;

        // UNSDERSCORE IS VALID IN ALL TYPES OF NUMBER VARIABLES
        int hex = 0xCAFE_BABE;
        float f = 9898_7878.333_333f;
        int bin = 0b1111_0000_1100_1100;

        double _100 = 1; // integer will be contravarianted to double
        double _101 = 1D;
        // Double _102 = 1; //compilation error // integer cannot be contravarianted to object Double
        Double _103 = 1D;
        System.out.println(_100);
    }

}
