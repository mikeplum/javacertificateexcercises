package Exams.Exam4.q44;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

public class CharSequenceTest {

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("Ala");
        sb.append("dssd",2,3);
        System.out.println(sb);
        sb.append(new char[]{'a', 'b', 'X'}, 1,2 );
        System.out.println(sb);
        sb.insert(2, "ELAELA", 4, "ELAELA".length()); // Throws: IndexOutOfBoundsException - if dstOffset is negative or greater than end.
        System.out.println(sb);
        sb.replace(sb.length()-1,sb.length(),"A"); //Throws: StringIndexOutOfBoundsException - if start is negative, greater than length(), or greater than end.
        System.out.println(sb);
        sb.substring(2,4);
        System.out.println(sb); // Wont print because substring method returns string - and String is immutable
        sb = new StringBuilder(sb.substring(2,4));
        System.out.println(sb); // Here we create an new String builder

    }

}
