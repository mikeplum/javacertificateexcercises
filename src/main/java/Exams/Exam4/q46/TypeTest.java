package Exams.Exam4.q46;

    class A{
        public static void main(String args[]){
            A a = new A();
            B b = new B();
            a = b;  // 1
            //b = a;  // Compilation error explicit cast needed
            a = (B) b; // 3
            b = (B) a; // 4
        }
    }

    class B extends A {

    }


