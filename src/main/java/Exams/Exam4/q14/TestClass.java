package Exams.Exam4.q14;


//What will the following program print?
public class TestClass{
    public static void main(String[] args){
        /* //for is a keyword it cannot be used as label
        for : for(int i = 0; i< 10; i++){
            for (int j = 0; j< 10; j++){
                if ( i+ j > 10 )  break for;
            }
            System.out.println( "hello");
        }
        */
        String String = "";   //This is valid.
        String : for(int i = 0; i< 10; i++) //This is valid too!
        {
            for (int j = 0; j< 10; j++){
                if ( i+ j > 10 )  break String;
            }
            System.out.println( "hello");
        }
    }

}