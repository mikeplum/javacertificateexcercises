package Exams.Exam4.q48;

import java.lang.reflect.Array;
import java.util.ArrayList;

class LoopTest{
    public static void main(String args[]) {
        int counter = 0;
        outer:
        for (int i = 0; i < 3; i++) {
            middle:
            for (int j = 0; j < 3; j++) {
                inner:
                for (int k = 0; k < 3; k++) {
                    if (k - j > 0) {
                        break middle;
                    }
                    counter++;
                }
            }
        }
        System.out.println(counter);


        Object b = new Object[]{ "aaa", new Object(), new ArrayList(), new String[]{""} };
        Object c = new Object[]{ "aaa", new Object(), new ArrayList(), 10};
        Object d = new Object[]{ "aaa", new Object(), new ArrayList(), new String[]{ } };
        Object e = new Object[]{ "aaa", new Object(), new ArrayList(), new String[]{null} };
        Object o = null;
     }
}