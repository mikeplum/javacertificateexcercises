package Exams.Exam4.q42;

public class Test {
    public void switchTest(byte x){
        switch(x){
            case 'b':   // 1
            default :   // 2
            case -2:    // 3
            case 80:    // 4
            case (byte) 12832:
            //case 12832: //above will not compile error this does becaues casting hasn't done
        }
    }
}
