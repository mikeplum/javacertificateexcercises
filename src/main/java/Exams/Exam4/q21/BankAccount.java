package Exams.Exam4.q21;

//Which statements about the following code contained in BankAccount.java are correct?

import Examples.a.AccessTest;

interface Account{
    public default String getId(){
        return "0000";
    }
}

interface PremiumAccount extends Account{
    public String getId();
}

public class BankAccount implements PremiumAccount{

    public static void main(String[] args) {
        Account acct = new BankAccount();
        System.out.println(acct.getId());
        System.out.println();
    }


    @Override
    public String getId() {
        return null;
    }

}
