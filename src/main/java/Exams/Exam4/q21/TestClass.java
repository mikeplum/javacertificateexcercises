package Exams.Exam4.q21;

interface TestInterface1
{
    // default method
    default void show()
    {
        System.out.println("Default TestInterface1");
    }
}

interface TestInterface2
{
    void dupa();

    static void dupa2() {
        System.out.println("Static TestInterface2");
    }

    // Default method
    default void show()
    {
        System.out.println("Default TestInterface2");
    }
}

// Implementation class code
class TestClass implements TestInterface1, TestInterface2
{
    @Override
    public void dupa() {
        TestInterface2.dupa2();
    }

    public void show() {
        TestInterface1.super.show();
    }

    public static void main(String args[])
    {
        TestClass d = new TestClass();
        d.show();
    }
}