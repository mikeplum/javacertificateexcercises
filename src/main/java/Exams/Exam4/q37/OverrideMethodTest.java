package Exams.Exam4.q37;

import java.io.IOException;

class SuperClass{
    public int getSet() throws IOException{
       final int a = 10;
       return a;
    }
}

abstract class OverrideMethodTestAbstract extends SuperClass{

}

public class OverrideMethodTest extends OverrideMethodTestAbstract{

    public static void main(String[] args) throws IOException {
        SuperClass d = new OverrideMethodTest();
        System.out.println(d.getSet());
    }

}