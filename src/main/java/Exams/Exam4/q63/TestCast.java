package Exams.Exam4.q63;

interface MyIface{};
class A { };
class B extends A implements MyIface {};
class C implements MyIface {};

public class TestCast {


    public static void main(String[] args) {

        A a = new A();
        B b = new B();
        C c = new C();
        //b = (B) a; //Explicit cast allow as to pass throug compilation error but it throw a ClassCastException at runtime since a is not obcjet of class B or it's subclasses
        System.out.println("done".charAt(43));
    }

}
