package Exams.Exam4.q7;


//Which of the following are correct ways to initialize the static variables MAX and CLASS_GUID ?
class Widget {
    static int MAX;     //1
    static final String CLASS_GUID;   // 2

    {
        MAX=221;
    }

    static{
        CLASS_GUID = "1112";
        MAX = 222;
    }

    Widget() { // This is not a static initializer and so will not be executed until an instance is created.
        MAX = 111;
        //CLASS_GUID = "XYZ123"; //this wont initialize CLASS_GUID this leaves compilation error in line // 2
    }

    Widget(int k) {
        //we cannot initialize static variables in non static method "non - Compilation error - nonStatic context
    }

    public void initMAX(){
        MAX = 431232132;
    }

    public final static void main(String[] dupad) {
        System.out.println(Widget.MAX); //0
        System.out.println(new Widget().MAX); //111
        System.out.println(Widget.MAX); //111
        Widget w = new Widget();
        w.initMAX();
        System.out.println(dupad[0]);
        System.out.println(w.MAX);
    }
}