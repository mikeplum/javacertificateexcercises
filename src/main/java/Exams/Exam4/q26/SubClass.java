package Exams.Exam4.q26;

public class SubClass extends SuperClass {
    int i, j, k;

    public SubClass(int m, int n) {
        //it call implicitly constructor super() "default" construtor needs to be
        //explicitly declared in SuperClass as long as it is constructor bellow declared as it is
        i = m;
        j = m;
    }

    public SubClass(int m) {
        super(m);
    }
    //Needed both in super class
    //public SuperClass()
    //public SuperClass(int a)
}