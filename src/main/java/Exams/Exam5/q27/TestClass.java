package Exams.Exam5.q27;

//Given the following program, which statements are true?

// Filename: TestClass.java
public class TestClass{
    public static void main(String args[]){
        A[] a, a1;
        B[] b;
        a = new A[10]; a1  = a;
        b =  new B[20];
        a = b;  // 1
        b = (B[]) a;  // 2
        b = (B[]) a1; // 3
        final StringBuffer sb = new StringBuffer("dsa");
        sb.reverse();
    }
}
class A { }
class B extends A { }
