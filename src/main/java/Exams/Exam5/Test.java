package Exams.Exam5;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

class A implements Runnable{
    @Override
    public void run() {

    }
}
class B extends A implements Observer {
    @Override
    public void update(Observable o, Object arg) {

    }
}
public class Test {
    static void main(String[] args) {
        java.time.LocalDate dt = java.time.LocalDate.parse("2015-01-01").minusMonths(1).minusDays(1).plusYears(1);
        A a = new A();
        B b = new B();
        Object o = a;
        Runnable r = (Runnable) o;
        //b =(B) a;
        Object o2 = b;
        Runnable r2 = b;

    }
}
