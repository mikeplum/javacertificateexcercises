package Exams.Exam5.q34;
//What will the following code print when run?

public class Noobs {

    public void l(long c){
        System.out.println("In char ");
    }

    public static void main(String[] args) {
        Noobs n = new Noobs();

        long a1 = 1;
        char a2=3254;
        int  a3 = 232332;
        short a4 = 232;
        byte a5 = (byte) 130;
        n.l(a1);
        n.l(a2);
        n.l(a3);
        n.l(a4);
        n.l(a5);
    }
}