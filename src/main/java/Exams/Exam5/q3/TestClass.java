package Exams.Exam5.q3;


public class TestClass {
    public static void main(String[] args) {
        short s =  32;

        //INVALID//Short k = new Short(9); System.out.println(k instanceof Short);
        //9 is considered an int and there is no constructor in Short that takes an int. Short s = new Short( (short) 9 ); will work.

        System.out.println(s == new Byte((byte)32));

    }
}
