package Exams.Exam5.q18;

//What will be the output when the following program is run?
public class TestClass{
    char c;
    public void m1(){
        char[ ] cA = { 'a' , 'b'};
        m2(c, cA);
        System.out.println( (c)  + "," + cA[1] );
    }
    public void m2(char d, char[ ] cA){
        c = 'b';
        cA[1] = cA[0] = 'm';
    }
    public static void main(String args[]){
        new TestClass().m1();
    }
}