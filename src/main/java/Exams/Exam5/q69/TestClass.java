package Exams.Exam5.q69;
//What will the following code print when compiled and run?

public class TestClass{
    public static void main(String[] args){
        int[] arr = { 1, 2, 3, 4, 5, 6 };
        int counter = 0;
        for (int value : arr) {
            if (counter >= 5) {
                break;
            } else{ //
                continue;
            }
            //code here is unrechable - compilation error if present
        }
        System.out.println(arr[counter]);
    }

}