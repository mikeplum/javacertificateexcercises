package Exams.Exam5.q17;
//Consider the following code:

class Super {
    static String ID;
    static {
        System.out.print("In Super");
    }
}

class Sub extends Super{
    static {
        System.out.print("In Sub");
    }
    public double methodX( byte by){
        double d = 10.0;
        return (long)(by/d*3);
    }
}

public class Test{
    private static double A;

    public static void main(String[] args){
        System.out.println(Sub.ID);
        //Sub s = new Sub();
        //Super s2 = new Super();
        //Super s3 = new Sub();

        System.out.println((long) 3/4);
    }
}

//What will be the output when class Test is run?