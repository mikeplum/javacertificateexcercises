package Exams.Exam5.q42;


import Examples.c.Ex;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
interface I{
}
class A implements I{
}

class B extends A {
}

class C extends B{
}
//Complete the following code by filling the two blanks -
class XXX{
    public void m() throws Exception{
        System.out.println("XXX metody executed");
        throw new Exception();
    }
}
class YYY extends XXX{
    public void m() {
        System.out.println("YYY metody executed");
    }

    public static void main(String[] args) throws Exception {
        XXX obj = new YYY();
        obj.m();
    }
}