package Exams.Exam6.q26;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

public class TestDatePrint {

    public static void main(String[] args) {
        System.out.println(LocalDateTime.of(2019,07,30, 20,04,50,3212).format(DateTimeFormatter.ISO_DATE_TIME));
    }

}
