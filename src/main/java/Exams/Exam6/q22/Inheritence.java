package Exams.Exam6.q22;

class A{
    public void m1() {
        System.out.println("A");
    }
}
class B extends A{
    public void m1() {
        System.out.println("B");
    }
}
class C extends B{
    public void m1(){

    }
    public void callAm1(){
        m1();
    }


    public static void main(String[] args) {
        C b = new C();
        b.m1();
    }
}
