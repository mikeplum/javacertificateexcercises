package Exams.Exam6.q2;


//Consider the following code:
interface Bar{
    void bar();
}

abstract class FooBase{

    private static void bar(){
        System.out.println("In static bar");
    }
}

public class Foo extends FooBase implements Bar {

    public static void main(String[] args) {
        Foo foo = new Foo();
        foo.bar();
        System.out.println();
        foo.bar();
    }

    @Override
    public void bar() {

    }
}
//What can be done to the above code so that it will compile without any error?