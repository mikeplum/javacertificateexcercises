package Exams.Exam6.q4;

import java.time.LocalDate;
import java.time.Period;

public class Test {

    public LocalDate process(LocalDate ld){
        LocalDate ld2 = ld.plus(Period.ofMonths(1).ofDays(1));
        return ld2;
    }

    public LocalDate processCorrect(LocalDate ld){
        LocalDate ld2 = ld.plus(Period.of(0,1,1));
        return ld2;
    }

    public static void main(String[] args) {
        Test t = new Test();
        System.out.println(t.process(LocalDate.of(2019,07,30)));
        System.out.println(t.processCorrect(LocalDate.of(2019,07,30)));
    }

}
