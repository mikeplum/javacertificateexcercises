package Exams.Exam6.q8;

//What will be printed when the following code is compiled and run?
class A {
    public int getCode(){ return 2;}
}

class AA extends A {
    //public long getCode(){ return 3;}// incorrect return type
}

public class TestClass {

    public static void main(String[] args) throws Exception {
        A a = new A();
        A aa = new AA();
        System.out.println(a.getCode()+" "+aa.getCode());
    }

    public int getCode() {
        return 1;
    }
}