package Exams.Exam6.q59;

import static Exams.Exam6.q52.Pow.wow;

import Exams.Exam6.q52.Pow;
import Exams.Exam6.q52.Powwow;
//Which statement regarding the following code is correct?

class A{
    public int i = 10;
    private int j = 20;

}

class B extends A{
    public int i = 30; //1
    public int k = 40;

}

class C extends B{
}

public class TestClass{
    public static void main(String args[]){
        C c = new C();
        System.out.println(((A)c).i);
        wow();
        byte starting = 3;
        short firstValue = 5;
        int secondValue = 7;
        long ds = 2;
        System.out.println(ds*23);
        int functionValue = (int) (starting/2 + firstValue/2 +  firstValue/3 ) + secondValue/2;
        System.out.println(functionValue);
        //What will the following code snippet print when compiled and run?
        short xx = (short) (starting+firstValue);

    }
}

