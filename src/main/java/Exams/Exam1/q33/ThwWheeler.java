package Exams.Exam1.q33;


interface Automobile { String describe(); }

class FourWheeler implements Automobile{
    protected String name = "FourWheeler";
    public String describe(){ return " 4 Wheeler " + name; }
}

class TwoWheeler extends FourWheeler{
    public String name = "TwoWheeler";
    public String describe(){ return " 2 Wheeler " + name; }
}