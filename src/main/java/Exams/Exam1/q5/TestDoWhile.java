package Exams.Exam1.q5;

public class TestDoWhile {

    public static void main(String[] args) throws Exception{
        int i = 1, j = 10;
        do {
            if (i++ > --j) continue;
        } while (i < 5);
        System.out.println("i=" + i + " j=" + j);

        int k = 10;

        if(k++==10){
            System.out.println("Dupa");
        }

    }
}
