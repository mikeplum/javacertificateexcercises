package Exams.Exam2.q4;

class MyException extends Throwable {
}

class MyException1 extends Throwable {
}

class MyException2 extends Throwable {
}

class MyException3 extends MyException {
}

public class ExceptionTest {
    void myMethod() throws MyException {
        throw new MyException3();
    }

    public int getSomeNumb() {
        Double d = Math.random();
        Integer i = d.intValue();
        return i;
    }

    public static void main(String[] args) {
        ExceptionTest et = new ExceptionTest();
        try {
            et.myMethod();
        } catch (MyException3 me3) {
            System.out.println("MyException3 thrown");
        } catch (MyException me) {
            System.out.println("MyException thrown");
        } finally {
            System.out.println(" Done");
        }
    }
}
