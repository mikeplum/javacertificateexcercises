package Exams.Exam2.q21q28;


public class SuperClass {

    public int primitive(int i) {
        return 1;
    }

    protected MyInteger0 primitive(double i) throws MyException {
        return new MyInteger0();
    }

    public static void main(String[] args) {

        int a1 = 1;
        Integer a2 = new Integer(1);
        System.out.println(a1==a2);
        System.out.println("======================1");

        Integer b1 = new Integer(1);
        Integer b2 = new Integer(1);
        System.out.println(b1==b2);
        System.out.println("======================2");

        Integer c1 = 127;
        Integer c2 = 127;
        System.out.println(c1==c2);
        System.out.println("======================3");

        Integer d1 = 128;
        Integer d2 = 128;
        System.out.println(d1==d2);
        System.out.println("======================4");

        int e1 = 127;
        int e2 = 127;
        System.out.println(e1==e2);
        System.out.println("======================5");

        int f1 = 129;
        int f2 = 129;
        System.out.println(f1==f2);
        System.out.println("======================6");

        MyInteger1 g1 = new MyInteger1();
        Exams.Exam2.abstractTest.MyInteger1 g2 = new Exams.Exam2.abstractTest.MyInteger1();
        System.out.println( g1.a == g2.a);
        System.out.println("======================7");

        MyInteger1 h1 = new MyInteger1();
        Exams.Exam2.abstractTest.MyInteger1 h2 = new Exams.Exam2.abstractTest.MyInteger1();
        System.out.println( h1.b == h2.b);
        System.out.println("======================8");

        MyInteger1 i1 = new MyInteger1();
        Exams.Exam2.abstractTest.MyInteger1 i2 = new Exams.Exam2.abstractTest.MyInteger1();
        System.out.println( i1.c == i2.c);
        System.out.println("======================9");

        Boolean bool = new Boolean("true");
        System.out.println(bool == Boolean.parseBoolean("true"));
        System.out.println("======================10");

        Boolean bool2 = new Boolean("true");
        System.out.println(bool2 == Boolean.TRUE);
        System.out.println("======================11");


    }

}
