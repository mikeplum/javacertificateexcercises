package Exams.Exam2.q44;

public class TestClass{

    private int x;

    int getX(){ return x; }

    public static void main(String[] args) throws Exception{
        TestClass tc = new TestClass();
        System.out.println(tc.x);
        tc.looper();
        System.out.println(tc.x);

    }

    public void looper(){
        int x = 0;
        while( (x = getX()) != 0 ){
            for(int m = 10; m>=0; m--){
                x = m;
            }
        }

    }
}