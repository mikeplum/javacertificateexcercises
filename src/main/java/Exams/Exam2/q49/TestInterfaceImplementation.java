package Exams.Exam2.q49;
public class TestInterfaceImplementation implements IInterface1, IInterface2{

    TestInterfaceImplementation(){}

    public void m1() { System.out.println("Hello"); }

    public static void main(String[] args){
        TestInterfaceImplementation tc = new TestInterfaceImplementation();
        int i = ((IInterface1) tc).VALUE;
        int j = tc.VALUE2;

        Object obj = new String("aaaa");
        Object obj2 = obj;
        obj = null;
        System.out.println(obj2);
    }
}
