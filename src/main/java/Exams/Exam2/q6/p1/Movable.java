package Exams.Exam2.q6.p1;

public interface Movable {
    public static final int location = 0;
    public void move(int by);
    public void moveBack(int by);

}
