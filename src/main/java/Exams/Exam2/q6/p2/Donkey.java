package Exams.Exam2.q6.p2;

import Exams.Exam2.q6.p1.Movable;

public class Donkey implements Movable {

    public int location = 200;

    public void move(int by) {
        location = location + by;
    }

    public void moveBack(int by) {
        location = location - by;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
          this.location = location;
    }

}