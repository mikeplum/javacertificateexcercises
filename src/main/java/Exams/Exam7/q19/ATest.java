package Exams.Exam7.q19;
//What will the following code print when compiled and run?

import Examples.c.Ex;

public class ATest {

    String global = "111";

    public int parse(String arg){
        int value = 0;
        try{
            String global = arg;
            value = Integer.parseInt(global);
            throw new Exception("ds");
        }
        catch(Exception e){
            System.out.println(e.getClass());
        }
        System.out.print(global+" "+value+" ");
        return value;
    }

    private static void change(Integer a){
        ++a;
    }

    public static void main(String[] args) {
        ATest ct = new ATest();
        System.out.print(ct.parse("333"));

        Integer i1 = new Integer(1);
        Integer i2 = i1;
        i1 = 2;
        System.out.println(i1 + " " + i2);

        int a1 = 1;
        Integer a2 = new Integer(2);
        change(a1);
        change(a2);
        System.out.println("==========="+a1 + " "+ a2);
        System.out.println(100/(13*2));
    }

}
